package de.hska.ss13.convexhull;

import java.util.ArrayList;

import de.hska.ss13.convexhull.model.CsvGenerator;
import de.hska.ss13.convexhull.model.GrahamScan;
import de.hska.ss13.convexhull.model.InputValues;
import de.hska.ss13.convexhull.model.MeasurementService;
import de.hska.ss13.convexhull.model.Point;
import de.hska.ss13.convexhull.model.PointGenerator;
import de.hska.ss13.convexhull.model.QuickHull;
import de.hska.ss13.convexhull.view.View;

/**
 * ConvexHull berechnet zu einer zufälligen Punktemenge deren konvexe Hülle. Das
 * Ergebnis wird in einem kleinen Fenster angezeigt.
 */
public class ConvexHull {

	/**
	 * Verzögerung zwischen den verschiedenen Punktmengen.
	 */
	private static final long WAIT_TIME = 1000;

	/**
	 * Standardabstand von Punkten zum Fensterrand.
	 */
	private static final int BORDER = 50;

	/**
	 * Startet das Programm.
	 * 
	 * @param args
	 *            Argumente
	 */
	public static void main(final String[] args) {
		GrahamScan model;
		QuickHull qhmodel;
		CsvGenerator csgen;
		View output;
		MeasurementService measurement;
		PointGenerator generator;
		InputValues inputValues;
		ArrayList<Point> points;

		qhmodel = new QuickHull();
		model = new GrahamScan();
		output = new View(model, qhmodel);
		model.addObserver(output);
		qhmodel.addObserver(output);
		csgen = new CsvGenerator();

		measurement = new MeasurementService();
		inputValues = new InputValues();
		generator = new PointGenerator(BORDER, output.getWidth() - BORDER,
				BORDER, output.getHeight() - BORDER);

		csgen.appendFile("pointCount; Quickhull; GrahamScan \n");

		// Punkte erzeugen mit zufaelligen Werten
		for (int pointCount : inputValues.getPointCounts()) {
			// Zufallspunkte erstellen
//			points = generator.generatePoints(pointCount);
			points = generator.generateRadialPoints(pointCount);

			// Fenstertitel anpassen
			output.setTitle("Konvexe Huelle - " + pointCount + "Punkte");

			// Algorithmus ausfuehren mit Zeitmessung
			measurement.start();
			qhmodel.runAlgorithm(points);
			measurement.stop();
			measurement.logResult(pointCount);
			long tmpTime = measurement.getDuration();

			try {
				Thread.sleep(WAIT_TIME/2);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			measurement.start();
			model.runAlgorithm(points);
			measurement.stop();
			measurement.logResult(pointCount);

			csgen.appendFile(pointCount, tmpTime, measurement.getDuration());

			// Pause
			try {
				Thread.sleep(WAIT_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		csgen.closeFile();
	}
}
