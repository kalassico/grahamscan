package de.hska.ss13.convexhull.view;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;

import de.hska.ss13.convexhull.model.GrahamScan;
import de.hska.ss13.convexhull.model.Point;
import de.hska.ss13.convexhull.model.QuickHull;


/**
 * Die View erzeugt eine primitive Ausgabe der Punkte und deren Hülle.
 */
public class View extends JFrame implements Observer {
	private static final long serialVersionUID = -858578139961519386L;
	private GrahamScan ghsModel;
	private QuickHull qhModel;
	private Ellipse2D.Double ellipse;
	private Graphics2D graphic;

	private final int DIAMETER = 4;

	public View(GrahamScan ghs, QuickHull qh){
		this.ghsModel = ghs;
		this.qhModel = qh;
		setTitle("Konvexe Huelle");
		setSize(1200, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setDefaultLookAndFeelDecorated(true);

		setVisible(true);
	}

	@Override
	public void paint(final Graphics g) {
		super.paint(g);
		graphic = (Graphics2D) g;

		ArrayList<Point> ghsAll = ghsModel.getAllPoints();
		ArrayList<Point> ghsHull = ghsModel.getHull();

		ArrayList<Point> qhAll = qhModel.getAllPoints();

		if (ghsHull != null && ghsAll != null && !ghsHull.isEmpty() && !ghsAll.isEmpty()) {
			drawAllPoints(ghsAll);
			drawHullPoints(ghsHull);
			drawOutline(ghsHull);
		}

		ArrayList<Point> qhHull = qhModel.getPoints();
		if (qhHull != null && !qhHull.isEmpty()) {
			drawAllPoints(qhAll);
			drawHullPoints(qhHull);
			drawOutline(qhHull);
		}
	}

	/**
	 * Alle Punkte in schwarz zeichnen.
	 * 
	 * @param all alle Punkte
	 */
	private void drawAllPoints(final ArrayList<Point> all) {
		graphic.setColor(Color.BLACK);
		ellipse = new Ellipse2D.Double();

		for (Point r : all) {
			ellipse.setFrame((int) r.getX() - (DIAMETER / 2), (int) r.getY()
					- (DIAMETER / 2), DIAMETER, DIAMETER);
			graphic.fill(ellipse);
			graphic.draw(ellipse);
		}
	}

	/**
	 * Alle Punkte der Hülle in rot zeichnen.
	 * 
	 * @param hull die Punkte der Hülle
	 */
	private void drawHullPoints(final ArrayList<Point> hull) {
		graphic.setColor(Color.RED);
		ellipse = new Ellipse2D.Double();

		for (Point r : hull) {
			ellipse.setFrame((int) r.getX() - (DIAMETER / 2), (int) r.getY()
					- (DIAMETER / 2), DIAMETER, DIAMETER);
			graphic.fill(ellipse);
			graphic.draw(ellipse);
		}
	}

	/**
	 * Die Punkte der Hülle mit Strinchen verbinden.
	 * 
	 * @param hull die Punkte der Hülle
	 */
	private void drawOutline(final ArrayList<Point> hull) {
		graphic.setColor(Color.RED);
		for (int i = 1; i <= hull.size() - 1; i++) {
			graphic.drawLine((int) hull.get(i - 1).getX(), (int) hull
					.get(i - 1).getY(), (int) hull.get(i).getX(), (int) hull
					.get(i).getY());
			if (i == 1) {
				graphic.drawLine((int) hull.get(hull.size() - 1).getX(),
						(int) hull.get(hull.size() - 1).getY(),
						(int) hull.get(0).getX(), (int) hull.get(0).getY());
			}
		}
	}

	@Override
	public void update(final Observable o, final Object arg) {
		repaint();
	}
}
