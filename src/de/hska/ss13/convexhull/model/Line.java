package de.hska.ss13.convexhull.model;

/**
 * Class contains line parameters.
 */
public class Line {


	/**
	 * X coordinate of the start point.
	 */
	private double x1Coord;

	/**
	 * X coordinate of the end point.
	 */
	private double x2Coord;

	/**
	 * Y coordinate of the start point.
	 */
	private double y1Coord;

	/**
	 * Y coordinate of the end point.
	 */
	private double y2Coord;
	
	/**
	 * coordinates of the start point
	 */
	private Point startPoint;
	
	/**
	 * coordinates of the end point
	 */
	private Point endPoint;

	public Line(Point start, Point end) {
		this.startPoint = start;
		this.endPoint = end;
	}

	/**
	 * Getter for {@link x1Coord}.
	 * @return the x1Coord
	 */
	public double getX1Coord() {
		return x1Coord;
	}

	/**
	 * Setter for {@link x1Coord}.
	 * @param x1Coord the x1Coord to set
	 */
	public void setX1Coord(double x1Coord) {
		this.x1Coord = x1Coord;
	}

	/**
	 * Getter for {@link x2Coord}.
	 * @return the x2Coord
	 */
	public double getX2Coord() {
		return x2Coord;
	}

	/**
	 * Setter for {@link x2Coord}.
	 * @param x2Coord the x2Coord to set
	 */
	public void setX2Coord(double x2Coord) {
		this.x2Coord = x2Coord;
	}

	/**
	 * Getter for {@link y1Coord}.
	 * @return the y1Coord
	 */
	public double getY1Coord() {
		return y1Coord;
	}

	/**
	 * Setter for {@link y1Coord}.
	 * @param y1Coord the y1Coord to set
	 */
	public void setY1Coord(double y1Coord) {
		this.y1Coord = y1Coord;
	}

	/**
	 * Getter for {@link y2Coord}.
	 * @return the y2Coord
	 */
	public double getY2Coord() {
		return y2Coord;
	}

	/**
	 * Setter for {@link y2Coord}.
	 * @param y2Coord the y2Coord to set
	 */
	public void setY2Coord(double y2Coord) {
		this.y2Coord = y2Coord;
	}

	/**
	 * Getter for {@link startPoint}
	 * @return the startPoint
	 */
	public Point getStartPoint() {
		return startPoint;
	}

	/**
	 * Setter for {@link startPoint}
	 * @param startPoint the startPoint to set
	 */
	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}

	/**
	 * Getter for {@link endPoint}
	 * @return the endPoint
	 */
	public Point getEndPoint() {
		return endPoint;
	}

	/**
	 * Setter for {@link endPoint}
	 * @param endPoint the endPoint to set
	 */
	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}
}
