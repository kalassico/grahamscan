package de.hska.ss13.convexhull.model;

import java.util.ArrayList;

/**
 * Klasse der Eingabewerte fuer die Algorithmen 
 */
public class InputValues {
	
	/**
	 * Anzahl der Punkte
	 */
	private ArrayList<Integer> pointCounts;

	/**
	 * Initialisierung der Punktezahlen
	 */
	public InputValues() {
		pointCounts = new ArrayList<>();
		pointCounts.add(1);
		pointCounts.add(2);
		pointCounts.add(250);
		pointCounts.add(500);
		pointCounts.add(1000);
		pointCounts.add(2000);
		pointCounts.add(4000);
		pointCounts.add(8000);
//		pointCounts.add(16000);
//		pointCounts.add(32000);
//		pointCounts.add(64000);
	}

	/**
	 * Getter for {@link pointCount}
	 * @return the pointCounts
	 */
	public ArrayList<Integer> getPointCounts() {
		return pointCounts;
	}
}
