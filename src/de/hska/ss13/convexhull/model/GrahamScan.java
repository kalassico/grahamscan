package de.hska.ss13.convexhull.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

/**
 * GrahamScan bietet die Funktionalitaet aus einer Menge von Punkten deren
 * konvexe Huelle zu berechnen. Fuer die Berechnung wird der Graham Scan
 * Algorithmus verwendet.
 */
public class GrahamScan extends Observable {

	/**
	 * Liste aller Punkte aus denen die Huelle bestimmt werden soll.
	 */
	private ArrayList<Point> allPoints;

	/**
	 * Liste aller Punkte, welche die Huelle definieren.
	 */
	private ArrayList<Point> hull;
	
	/**
	 * Liste f�r das Kopieren der Punkte
	 */
	private ArrayList<Point> tempList;
	
	/**
	 * Fuehrt den Graham Scan Algorithmus auf die übergebenen Punkte aus und
	 * speichert das Ergebnis in {@link #hull}.
	 * 
	 * @param points
	 *            Punktemenge
	 */
	public void runAlgorithm(ArrayList<Point> points) {
		Point basis = getBottomPoint(points);

		allPoints = new ArrayList<>(points);
		ArrayList<Point> candidates = points;
		tempList = new ArrayList<>(points.size());
		
		Collections.sort(candidates, new ComparatorAngle(basis));

		int i = 1;
		int j = 1;
		if(points.size() > 2){
			tempList.add(candidates.get(i-1));
			tempList.add(candidates.get(i));
			tempList.add(candidates.get(i+1));
		}else{
			tempList = points;
		}
		while ((i + 1) < (candidates.size())) {
			boolean isRight = rightTurn(tempList.get(j-1), 
					tempList.get(j), tempList.get(j+1));
			if (!isRight) {
				i++;
				j++;
				tempList.add(candidates.get(i));
			} else {
				tempList.remove(j);
				j--;
			}
		}
		hull = tempList;
		setChanged();
		notifyObservers();
	}
	
//	public void runAlgorithm(final ArrayList<Point> points) {
//        Point basis = getBottomPoint(points);
//
//        allPoints = new ArrayList<>(points);
//        ArrayList<Point> candidates = points;
//
//        Collections.sort(candidates, new ComparatorAngle(basis));
//
//        int i = 1;
//        while ((i + 1) < (candidates.size())) {
//                boolean isRight = rightTurn(candidates.get(i - 1),
//                                candidates.get(i), candidates.get(i + 1));
//                if (!isRight) {
//                        i++;
//                } else {
//                        candidates.remove(i);
//                        i--;
//                }
//        }
//        hull = candidates;
////      setChanged();
////      notifyObservers();
//}
	
	/**
	 * Gibt den Punkt zurueck der den hoechsten Y-Wert aufweist.
	 * 
	 * @param points
	 *            Punktemenge
	 * @return Punkt mit dem groessten Y-Wert in der uebergebenen Punktemenge.
	 */
	private Point getBottomPoint(final ArrayList<Point> points) {
		Point bottom = points.get(0);
		for (Point p : points) {
			if (p.getY() > bottom.getY()) {
				bottom = p;
			}
		}
		return bottom;
	}

	/**
	 * Prueft ob ein Punkt links oder rechtsseitig einer
	 * Verbindung zweier Punkte liegt.
	 * @param a Startpunkt der Linie
	 * @param b Endpunkt der Linie
	 * @param c zu pruefender Punkt
	 * @return 	0 wenn Punkt c auf der Verbindung
	 * 				zwischen a und b liegt
	 * 			1 wenn Punkt c rechts der Verbindung liegt
	 */
	private boolean rightTurn(final Point a, final Point b, final Point c) {
		return (b.getX() - a.getX()) * (c.getY() - a.getY())
				- (b.getY() - a.getY()) * (c.getX() - a.getX()) > 0;
	}

	/**
	 * Getter for {@link #allPoints}.
	 * 
	 * @return Liste aller Punkte
	 */
	public ArrayList<Point> getAllPoints() {
		return allPoints;
	}

	/**
	 * Getter for {@link #hull}.
	 * 
	 * @return Liste der auf der Huelle liegenden Punkte
	 */
	public ArrayList<Point> getHull() {
		return hull;
	}
	
}
