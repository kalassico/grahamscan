package de.hska.ss13.convexhull.model;

/**
 * Zeitmessung stellt Funktionen zur Verfügung die eine Laufzeitanalyse
 * ermöglichen.
 */
public class MeasurementService {
	
	/**
	 * Beginn der Zeitmessung.
	 */
	private long startTime;

	/**
	 * Ende der Zeitmessung.
	 */
	private long endTime;

	/**
	 * Dauer der Zeitmessung.
	 */
	private long duration;

	/**
	 * Startet die Zeitmessung.
	 */
	public void start() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Beendet die Zeitmessung.
	 */
	public void stop() {
		endTime = System.currentTimeMillis();
		duration = endTime - startTime;
	}

	/**
	 * Loggt die Ergebnisse.
	 * 
	 * @param pointCount
	 *            Anzahl an Punkte die verarbeitet wurden
	 */
	public void logResult(final int pointCount) {
		System.out.println("Anzahl Punkte: " + pointCount + " Laufzeit: "
				+ duration + "ms");
	}
	
	/**
	 * Getter for {@link duration}
	 * @return Zeitwert des Algorithmus
	 */
	public long getDuration(){
		return this.duration;
	}

}
