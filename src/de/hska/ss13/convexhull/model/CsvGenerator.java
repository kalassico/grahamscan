package de.hska.ss13.convexhull.model;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Implementierung des Generators fuer CSV Dateien.
 */
public class CsvGenerator {

	/**
	 * Schreiber fuer die Ausgabedatei.
	 */
	private FileWriter writer;

	/**
	 * <<Constructor>>
	 * Initialisierung des Schreibers.
	 */
	public CsvGenerator(){
		try {
			this.writer = new FileWriter("Documentation/data.csv");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Beendet das Schreiben und schlie�t die Ausgabedatei.
	 */
	public void closeFile(){
		try {
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Fuegt der Ausgabedatei eine Zeile hinzu und bricht um.
	 * @param pointCount Anzahl der Punkte
	 * @param tmpTime	Zeit des GrahamScan Algorithmus
	 * @param duration Zeit des QuickHull Algorithmus
	 */
	public void appendFile(int pointCount, long tmpTime, long duration) {
		try {
			writer.append(String.valueOf(pointCount));
			writer.append(';');
			writer.append(String.valueOf(tmpTime));
			writer.append(';');
			writer.append(String.valueOf(duration));
			writer.append('\n');
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Fuegt der Datei eine Zeile hinzu. 
	 * Wird verwendet fuer Ueberschriften
	 * @param string die zu schreibende Zeile
	 */
	public void appendFile(String string) {
		try {
			writer.append(string);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
