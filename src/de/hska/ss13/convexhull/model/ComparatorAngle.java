package de.hska.ss13.convexhull.model;

import java.util.Comparator;

/**
 * ComperatorAngle vergleicht Punkte anhand ihrer Winkel, die eine Gerade durch
 * sie selbst und den Ausgangspunkt liefern.
 */
public class ComparatorAngle implements Comparator<Point> {

	/**
	 * Ausgangspunkt von dem die Gerade erstellt wird.
	 */
	private Point basis;

	/**
	 * Erzeugt einen neuen Comperator zum übergebenen Basispunkt.
	 * 
	 * @param basisPoint
	 *            Basispunkt
	 */
	public ComparatorAngle(final Point basisPoint) {
		this.basis = basisPoint;
	}

	/**
	 * Vergleicht die Laenge zweier gegebenen Punkte zu Basis
	 */
	@Override
	public int compare(final Point arg0, final Point arg1) {
		double angleArg0 = Math.abs(Math.toDegrees(Math.atan2(arg0.getY()
				- basis.getY(), arg0.getX() - basis.getX())));
		double angleArg1 = Math.abs(Math.toDegrees(Math.atan2(arg1.getY()
				- basis.getY(), arg1.getX() - basis.getX())));

		return Double.compare(angleArg0, angleArg1);
	}

}
