package de.hska.ss13.convexhull.model;

/**
 * Ein Punkt ist ein Wert in 2-dimensionalen-Raum.
 */
public class Point {

	/**
	 * Die X-Koordninate des Punktes.
	 */
	private double x;

	/**
	 * Die Y-Koordniate des Punktes.
	 */
	private double y;

	/**
	 * Erzeugt einen neuen Punkt mit übergebenen X- und Y-Werten.
	 * 
	 * @param xValue
	 *            X-Koordniate
	 * @param yValue
	 *            Y-Koordniate
	 */
	public Point(final double xValue, final double yValue) {
		this.x = xValue;
		this.y = yValue;
	}

	/**
	 * Getter für {@link #x}.
	 * 
	 * @return x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Setter für {@link #x}.
	 * 
	 * @param x
	 *            X-Wert
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Getter für {@link #y}.
	 * 
	 * @return y
	 */
	public double getY() {
		return y;
	}

	/**
	 * Setter für {@link #x}.
	 * 
	 * @param y
	 *            Y-Wert
	 */
	public void setY(double y) {
		this.y = y;
	}

}
