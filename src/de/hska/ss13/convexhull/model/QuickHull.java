package de.hska.ss13.convexhull.model;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Implementierung des QuickHull Algorithmus.
 */
public class QuickHull extends Observable {

	/**
	 * Liste aller Punkte.
	 */
	private ArrayList<Point> points;

	/**
	 * Liste aller Punkte auf der Huelle.
	 */
	private ArrayList<Point> hullPoints;

	/**
	 * Konstruktor der Klasse Quickhull. Initialisiert die Listen fuer Punkte.
	 */
	public QuickHull() {
		points = new ArrayList<>();
		hullPoints = new ArrayList<>();
	}

	/**
	 * Fuehrt den QuckHull Algorithmus aus und speichert das Ergebnis in
	 * {@link #hullPoints}.
	 * 
	 * @param points
	 *            Liste aller Punkte
	 */
	public void runAlgorithm(final ArrayList<Point> points) {
		this.points = points;
		this.hullPoints.clear();

		Point min_x = getLeftPoint(points);
		Point max_x = getRightPoint(points);

		ArrayList<Point> subset1 = createSubSet(min_x, max_x, points);
		ArrayList<Point> subset2 = createSubSet(max_x, min_x, points);
		hullPoints.add(min_x);
		hullPoints.addAll(findHull(min_x, max_x, subset1));
		hullPoints.add(max_x);
		hullPoints.addAll(findHull(max_x, min_x, subset2));

//		 setChanged();
//		 notifyObservers();
	}

	/**
	 * Rekursive Methode fuer die Findung der konvexen Menge.
	 * 
	 * @param pointA
	 *            Startpunkt der Trennlinie
	 * @param pointB
	 *            Endpunkt der Trennlinie
	 * @param points
	 *            Teilliste links verbleibender Punkte
	 * @return Liste von Punkten auf der konvexen Huelle
	 */
	private ArrayList<Point> findHull(Point pointA, Point pointB,
			ArrayList<Point> points) {
		ArrayList<Point> hPoints = new ArrayList<Point>();
		if (points.size() != 0) {
			
			Point pointC = findBiggestArea(pointA, pointB, points);

			ArrayList<Point> subsetA = new ArrayList<Point>();
			ArrayList<Point> subsetB = new ArrayList<Point>();

			subsetA = createSubSet(pointA, pointC, points);
			subsetB = createSubSet(pointC, pointB, points);

			hPoints.addAll(findHull(pointA, pointC, subsetA));
			hPoints.add(pointC);
			hPoints.addAll(findHull(pointC, pointB, subsetB));
		}
		return hPoints;
	}

	/**
	 * Erstellt die Untermengen fuer die weitere Verarbeitung
	 * 
	 * @param start
	 *            Startpunkt der Trennlinie
	 * @param end
	 *            EndPunkt der Trennlinie
	 * @param points
	 *            Liste der verbleibenden Punkte
	 * @return Liste von Punkten die links der Trennlinie liegen
	 */
	private ArrayList<Point> createSubSet(Point start, Point end,
			ArrayList<Point> points) {
		ArrayList<Point> subset = new ArrayList<>();
		for (Point p : points) {
			if (isLeft(start, end, p) > 0) {
				if (!points.equals(start) && !points.equals(end)) {
					subset.add(p);
				}
			}
		}
		return subset;
	}

	/**
	 * Prueft ob sich ein Punkt links einer Trennlinie befindet
	 * 
	 * @param p0
	 *            Der zu testende Punkt
	 * @param p1
	 *            Startpunkt der Trennlinie
	 * @param p2
	 *            Endpunkt der Trennlinie
	 * @return -wert wenn Punkt links der Linie
	 */
	private double rightTurn(Point a, Point b, Point c) {
		return (b.getX() - a.getX()) * (c.getY() - a.getY())
				- (b.getY() - a.getY()) * (c.getX() - a.getX());
	}

	/**
	 * Bestimmt die gr��te Dreiecksflaeche
	 * 
	 * @param start
	 *            Startpunkt der Trennlinie
	 * @param end
	 *            Endpunkt der Trennlinie
	 * @param points
	 *            Liste verbleibender Punkte
	 * @return point Punkt welcher mit start und end die groe�te Dreiecksflaeche
	 *         bildet.
	 */
	private Point findBiggestArea(Point start, Point end,
			ArrayList<Point> points) {
		Point point = points.get(0);
		double area = Double.MIN_VALUE;
		if(points.size() == 1){
			return points.get(0);
		}
		for (Point p : points) {
			double ar = determinante(start, end, p);
			if (ar > area) {
				area = ar;
				point = p;
			}
		}
		return point;
	}

	/**
	 * Bildet die Determinante von drei gegebenen Punkten
	 * 
	 * @param p1
	 *            Punkt eins des Dreiecks
	 * @param p2
	 *            Punkt zwei des Dreiecks
	 * @param p3
	 *            Punkt drei des Dreiecks
	 * @return determinate Berechnete Determinante
	 */
	private double determinante(Point p1, Point p2, Point p3) {
		double det = 0;
		det = Math.abs((p1.getX() * p2.getY()) + (p3.getX() * p1.getY())
				+ (p2.getX() * p3.getY()) - (p3.getX() * p2.getY())
				- (p2.getX() * p1.getY()) - (p1.getX() * p3.getY())) / 2;
		return det;
	}
	
	/**
	 * Gibt den Punkt zurueck der den hoechsten Y-Wert aufweist.
	 * 
	 * @param points
	 *            Punktemenge
	 * @return Punkt mit dem groessten Y-Wert in der uebergebenen Punktemenge.
	 */
	private Point getLeftPoint(ArrayList<Point> points) {
		Point bottom = points.get(0);
		for (Point p : points) {
			if (p.getX() < bottom.getX()) {
				bottom = p;
			}
		}
		return bottom;
	}

	/**
	 * Gibt den Punkt zurueck der den niedrigsten y-Wert aufzeigt
	 * 
	 * @param points
	 *            Punktemenge
	 * @return Punkt mit dem kleinsten y-Wert in der Punktemenge
	 */
	private Point getRightPoint(ArrayList<Point> points) {
		Point top = points.get(0);
		for (Point p : points) {
			if (p.getX() > top.getX()) {
				top = p;
			}
		}
		return top;
	}

	/**
	 * Getter for {@link hullPoints}
	 * 
	 * @return Liste der konvexen menge
	 */
	public ArrayList<Point> getPoints() {
		return this.hullPoints;
	}

	/**
	 * Getter for {@link points}.
	 * 
	 * @return Liste aller Punkte
	 */
	public ArrayList<Point> getAllPoints() {
		return this.points;
	}

}
