package de.hska.ss13.convexhull.model;

import java.util.ArrayList;

/**
 * Der PointGenerator erzeugt eine Menge von zufällig platzierten Punkten. Der
 * Wertebreich kann dabei eingeschränkt werden.
 */
public class PointGenerator {

	/**
	 * Der minimale X-Wert den ein Punkt annehmen darf.
	 */
	private int xMin;

	/**
	 * Der maximale X-Wert den ein Punkt annehmen darf.
	 */
	private int xMax;

	/**
	 * Der minimale Y-Wert den ein Punkt annehmen darf.
	 */
	private int yMin;

	/**
	 * Der maximale Y-Wert den ein Punkt annehmen darf.
	 */
	private int yMax;

	/**
	 * Radius f�r die Generierung der Kreispunkte.
	 */
	private static final int RADIUS = 300;

	/**
	 * Verschiebung des Kreismittelpunktes.
	 */
	private static final int RADIAL_OFFSET = 500;

	/**
	 * Erzeugt einen neuen PointGenerator mit uebergebenen
	 * Wert-Restriktionen.
	 * @param xMinValue
	 *            minimaler X-Wert
	 * @param xMaxValue
	 *            maximaler X-Wert
	 * @param yMinValue
	 *            minimaler Y-Wert
	 * @param yMaxValue
	 *            maximaler Y-Wert
	 */
	public PointGenerator(final int xMinValue, final int xMaxValue,
			final int yMinValue, final int yMaxValue) {
		this.xMin = xMinValue;
		this.xMax = xMaxValue;
		this.yMin = yMinValue;
		this.yMax = yMaxValue;
	}

	/**
	 * Erzeugt <code>count</code> zufällige Punkte.
	 * @param count
	 *            Anzahl zu erstellenden Punkten
	 * @return ArrayList mit <code>count</code> Zufallspunkten.
	 */
	public final ArrayList<Point> generatePoints(final int count) {
		ArrayList<Point> points = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			points.add(new Point(xMin + (float) Math.random()
					* (xMax - xMin), yMin
					+ (float) Math.random()
					* (yMax - yMin)));
		}
		return points;
	}

	/**
	 * Erzeugt <code>count</code> zufaellige Punkte auf einem Kreis.
	 * @param count Anzahl der zu erstellenden Punkte
	 * @return ArrayList mit <code>count</code> Punkten.
	 */
	public final ArrayList<Point> generateRadialPoints(final int count) {
		ArrayList<Point> points = new ArrayList<>();
		float angle = 0;
		for (int i = 0; i < count; i++) {
			points.add(new Point(RADIAL_OFFSET + (Math.cos(i * angle) * RADIUS)
					, RADIAL_OFFSET + (Math.sin(i * angle) * RADIUS)));
			angle+=30;
		}
		return points;
	}

}
