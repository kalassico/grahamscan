package de.hska.ss13.grahamscan;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Test;

import de.hska.ss13.convexhull.model.Point;
import de.hska.ss13.convexhull.model.QuickHull;

/**
 * Testklasse f�r den QuickHull Algorithmus
 */
public class QuickHullTest {

	@Test
	public void test() {

		ArrayList<Point> points = new ArrayList<>();

		Point topLeft = new Point(100, 100);
		Point topRight = new Point(200, 200);
		Point bottemLeft = new Point(100, 300);
		Point bottomRight = new Point(250, 400);
		Point middleRight = new Point(150, 120);
		Point innerPointRight = new Point(170, 200);

		points.add(topLeft);
		points.add(topRight);
		points.add(bottemLeft);
		points.add(bottomRight);
		points.add(middleRight);
		points.add(innerPointRight);

		QuickHull qh = new QuickHull();

		int pointcount = 6;
		int result = 5;

		Assert.assertEquals(points.size(), pointcount);
		qh.runAlgorithm(points);
		ArrayList<Point> resultList = qh.getPoints();
		Assert.assertEquals(result, (resultList.size()));
		Assert.assertTrue(qh.getPoints().contains(topLeft));
		Assert.assertTrue(qh.getPoints().contains(bottemLeft));
		Assert.assertTrue(qh.getPoints().contains(topRight));
		Assert.assertTrue(qh.getPoints().contains(bottomRight));
		Assert.assertTrue(qh.getPoints().contains(middleRight));
		Assert.assertFalse(qh.getPoints().contains(innerPointRight));
	}
	
}
