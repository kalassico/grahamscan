package de.hska.ss13.grahamscan;

import junit.framework.Assert;

import org.junit.Test;

import de.hska.ss13.convexhull.model.Point;

public class PointTest {

	@Test
	public void test() {

		Point p = new Point(5f, 10f);

		Assert.assertEquals(5f, p.getX(), 0);
		Assert.assertEquals(10f, p.getY(), 0);

		p.setX(7f);
		p.setY(19f);

		Assert.assertEquals(7f, p.getX(), 0);
		Assert.assertEquals(19f, p.getY(), 0);

	}

}