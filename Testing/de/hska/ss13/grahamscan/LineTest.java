package de.hska.ss13.grahamscan;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import de.hska.ss13.convexhull.model.Line;
import de.hska.ss13.convexhull.model.Point;

public class LineTest {

	@Test
	public void test() {
		
		Point start = new Point(10f, 10f);
		Point end = new Point(50f, 50f);
		
		Line l = new Line(start,end);
		
		l.setStartPoint(start);
		l.setEndPoint(end);
		l.setX1Coord(10f);
		l.setX2Coord(50f);
		l.setY1Coord(10f);
		l.setY2Coord(50f);
		
		Assert.assertEquals(start, l.getStartPoint());
		Assert.assertEquals(end, l.getEndPoint());
		Assert.assertEquals(10f, l.getX1Coord(), 0);
		Assert.assertEquals(50f, l.getX2Coord(), 0);
		Assert.assertEquals(10f, l.getY1Coord(), 0);
		Assert.assertEquals(50f, l.getY2Coord(), 0);
		
		
		
		
		
		
		
	}

}
