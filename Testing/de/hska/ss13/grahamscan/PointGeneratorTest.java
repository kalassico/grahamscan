package de.hska.ss13.grahamscan;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Test;

import de.hska.ss13.convexhull.model.Point;
import de.hska.ss13.convexhull.model.PointGenerator;

public class PointGeneratorTest {

	@Test
	public void test() {
		ArrayList<Point> points = new ArrayList<Point>();
		int xMinValue = 10;
		int xMaxValue = 100;
		int yMinValue = 10;
		int yMaxValue = 10;
		PointGenerator pg = new PointGenerator(xMinValue, xMaxValue, yMinValue, yMaxValue);

		 int pointCount = 2000;

		 points = pg.generatePoints(pointCount);
		 Assert.assertEquals(pointCount, points.size());
		 
		 points = pg.generateRadialPoints(pointCount);
		 Assert.assertEquals(pointCount, points.size());
	}

}
