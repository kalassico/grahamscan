package de.hska.ss13.grahamscan;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Test;

import de.hska.ss13.convexhull.model.GrahamScan;
import de.hska.ss13.convexhull.model.Point;

/**
 * Testet den eigentlichen GrahamScan.
 * @author martin
 *
 */
public class GrahamScanTest {

	@Test
	public void test() {
		ArrayList<Point> points = new ArrayList<>();

		final float high = 100f;
		final float low = 1f;
		final float mid = 50f;

		final int pointSize = 5;
		final int hullSize = 4;

		Point leftHigh = new Point(low, low);
		Point leftLow = new Point(low, high);
		Point rightLow = new Point(high, high);
		Point rightHigh = new Point(high, low);
		Point innerPoint = new Point(mid, mid);


		points.add(leftHigh);
		points.add(leftLow);
		points.add(rightLow);
		points.add(rightHigh);
		points.add(innerPoint);

		GrahamScan gs = new GrahamScan();

		Assert.assertEquals(pointSize, points.size());
		gs.runAlgorithm(points);
		Assert.assertEquals(hullSize, gs.getHull().size());
		Assert.assertTrue(gs.getHull().contains(leftHigh));
		Assert.assertTrue(gs.getHull().contains(leftLow));
		Assert.assertTrue(gs.getHull().contains(rightHigh));
		Assert.assertTrue(gs.getHull().contains(rightLow));
		Assert.assertFalse(gs.getHull().contains(innerPoint));
	}
	
	
	@Test 
	public void TestOnePoint (){
		ArrayList<Point> points = new ArrayList<>();
		Point p1 = new Point(100,100);
		Point p2 = new Point(240,230);
		
		points.add(p1);
		points.add(p2);
		
		GrahamScan gs = new GrahamScan();
		
		gs.runAlgorithm(points);
		Assert.assertTrue(gs.getHull().contains(p1) );
		Assert.assertNotNull(gs.getAllPoints());
		Assert.assertTrue(gs.getAllPoints().size()==2);
	}

}
