package de.hska.ss13.grahamscan;

import junit.framework.Assert;

import org.junit.Test;

import de.hska.ss13.convexhull.model.ComparatorAngle;
import de.hska.ss13.convexhull.model.Point;

/**
 * Testet die Comperator der Punkte nach Winkel zu einer Basis sortiert.
 */
public class ComparatorAngleTest {

	@Test
	public void testCompare() {

		Point basis = new Point(0f, 0f);
		Point low = new Point(10f, 10f);
		Point high = new Point(100f, 200f);

		ComparatorAngle test = new ComparatorAngle(basis);

		Assert.assertEquals(-1, (test.compare(low, high)));
		Assert.assertEquals(0, (test.compare(low, low)));
		Assert.assertEquals(1, (test.compare(high, low)));

	}
}